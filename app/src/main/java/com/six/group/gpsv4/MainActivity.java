package com.six.group.gpsv4;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.robolectric.annotation.Implements;

@Implements
public class MainActivity extends AppCompatActivity implements LocationListener {

    private double latitude = 45;
    private double longitude = 21;
    private Position position;

    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        locationManager =
                (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(locationManager.GPS_PROVIDER)) {
            Intent gpsOptions = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(gpsOptions);
        }

        locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER, 2000, 50, this);

        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText("Waiting for location...");

        position = new Position();
        //deviceID
        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        position.setDeviceID(android_id);

    }

    private void sendPositionUpdate(double latitude, double longitude) {
        //latitude
        position.setLatitude(new Double(latitude).toString());
        //longtitude
        position.setLongtitude(new Double(longitude).toString());
        // String jsonString = gson.toJson(position);
        if (position.sendPosition(position).getStatusLine().getStatusCode() != 200) {
            Toast.makeText(MainActivity.this, "Woops - Cannot connect to Server", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(MainActivity.this, "Position Sent", Toast.LENGTH_LONG).show();

        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public void openInMaps(View view) {
        Intent mapsIntent = new Intent(MainActivity.this, MapsActivity.class);

        mapsIntent.putExtra("KEY_latitude", latitude);
        mapsIntent.putExtra("KEY_longitude", longitude);

        startActivity(mapsIntent);
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();

        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText("\nLatitude: " + latitude
                + " Longitude: " + longitude + "");

        sendPositionUpdate(latitude, longitude);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
