package com.six.group.gpsv4;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.google.maps.android.heatmaps.WeightedLatLng;

import org.apache.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    private double latitude;
    private double longitude;
    Marker marker;
    private LocationManager locationManager;
    private Position position;
    private TileOverlay mOverlay;
    private HeatmapTileProvider mProvider;
    private TextView propertyInfo;
    LinkedList<Property> propertyList = new LinkedList<>();
    Property selectedProperty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        locationManager = (LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
//        if (!locationManager.isProviderEnabled(locationManager.GPS_PROVIDER)) {
//            Intent gpsOptions = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//            startActivity(gpsOptions);
//        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER, 10000, 200, this);
        locationManager.requestLocationUpdates(locationManager.NETWORK_PROVIDER, 10000, 200, this);

        position = new Position();
        //deviceID
        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        position.setDeviceID(android_id);
        propertyInfo = (TextView)findViewById(R.id.propertyInfo);
    }

    private void initMapOverlay() {
        List<WeightedLatLng> data = getLatLongData();

        mProvider = new HeatmapTileProvider.Builder()
                .weightedData(data)
                .build();
        mProvider.setRadius(50);
        mMap.animateCamera(CameraUpdateFactory.zoomTo(18.0f));
        mOverlay = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
    }

    private List<WeightedLatLng> sendPositionUpdate(double latitude, double longitude) throws IOException, JSONException {
        //latitude
        position.setLatitude(new Double(latitude).toString());
        //longtitude
        position.setLongtitude(new Double(longitude).toString());
        // String jsonString = gson.toJson(position);
        HttpResponse response = position.sendPosition(position);
        if (response.getStatusLine().getStatusCode() != 200) {
            Toast.makeText(MapsActivity.this, "Woops - Cannot connect to Server", Toast.LENGTH_SHORT).show();
            return new LinkedList<>();
        } else {
            Toast.makeText(MapsActivity.this, "Position Sent", Toast.LENGTH_SHORT).show();
            return readItems(response.getEntity().getContent());
        }
    }

    private ArrayList<WeightedLatLng> readItems(InputStream is) throws JSONException {
        ArrayList<WeightedLatLng> list = new ArrayList<>();
        String json = new Scanner(is).useDelimiter("\\A").next();
//        String json = "[" +
//        "{\"lat\" : -37.1886, \"lng\" : 145.708, \"weight\" : 1.0 } ," +
//            "{\"lat\" : -37.8361, \"lng\" : 144.845, \"weight\" : 0.1 } ," +
//                "{\"lat\" : -38.4034, \"lng\" : 144.192,\"weight\" : 0.2 } ," +
//                    "{\"lat\" : -38.7597, \"lng\" : 143.67,\"weight\" : 0.3 } ," +
//                        "{\"lat\" : -36.9672, \"lng\" : 141.083,\"weight\" : 0.05 }" +
//                            "]";
        JSONArray array = new JSONArray(json);
        double maxPrice = 0;
        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);
            double price = object.getDouble("price");
            if (price > maxPrice) {
                maxPrice = price;
            }
            propertyList.add(new Property(
                    object.getDouble("latitude"),
                    object.getDouble("longitude"),
                    object.getDouble("price"),
                    0.0,
                    object.getString("address"),
                    object.getString("postcode")
            ));
        }
        for (Property property : propertyList) {
            property.weight = property.price / maxPrice;
//            property.lat = ((new Random().nextDouble() * 14313)/100000) + 50.832704;
//            property.lng = ((new Random().nextDouble() * -22746)/1000000) -0.128135;
            //Log.d("INFO", "LAT: " + property.lat + "| LNG: " + property.lng);
            list.add(new WeightedLatLng(new LatLng(property.lat, property.lng), property.weight));
        }
        return list;
    }

    public void goBack(View view) {
        this.finish();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                selectedProperty = getClosestProperty(latLng);
                if (propertyList != null && selectedProperty != null) {
                    propertyInfo.setText("Address: " + selectedProperty.address
                            + "\nPrice: " + selectedProperty.price);
                    if (marker != null) {
                        marker.remove();
                    }
                    marker = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(selectedProperty.lat, selectedProperty.lng)));

                }

            }
        });
//        setLatitude(getIntent().getIntExtra("KEY_latitude"));
//        setLongitude(getIntent().getIntExtra("KEY_longitude", 21));
        // Add a marker in Sydney and move the camera
        updatePosition();
        initMapOverlay();
    }

    private Property getClosestProperty(LatLng latLng) {
        Location location = new Location("temp");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);

        float meters = 999999999;

        Property temp = null;

        for(Property property :  propertyList) {
            Location pLocation = new Location("pLocation");
            pLocation.setLatitude(property.lat);
            pLocation.setLongitude(property.lng);
            if (location.distanceTo(pLocation) < meters) {
                temp = property;
                meters = location.distanceTo(pLocation);
            }
        }

        return temp;
    }


    private void updatePosition() {
        if (marker != null) {
            marker.remove();
        }
        LatLng position = new LatLng(getLatitude(), getLongitude());
        mMap.animateCamera(CameraUpdateFactory.zoomTo(19.0f));

        //marker = mMap.addMarker(new MarkerOptions().position(position).title("Your position"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(position));
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("LOCATION", "LOCATION CHANGED");
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        updatePosition();
        try {
            List<WeightedLatLng> list = sendPositionUpdate(latitude, longitude);
            mProvider.setWeightedData(list);
            mOverlay.clearTileCache();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public List<WeightedLatLng> getLatLongData() {

        List<WeightedLatLng> list = new LinkedList<>();
        list.add(new WeightedLatLng(new LatLng(21,21), 0.6));

        return list;
    }

    public void propertyInfo(View view) {
        Intent graph = new Intent(MapsActivity.this, GraphActivity.class);

        graph.putExtra("address", selectedProperty.address);
        graph.putExtra("lat", selectedProperty.lat);
        graph.putExtra("lng", selectedProperty.lng);
        graph.putExtra("price", selectedProperty.price);



        startActivity(graph);
//        FragmentManager fragmentManager = getFragmentManager();
////        GraphActivity graphActivityFragment = (GraphActivity)fragmentManager.findFragmentById(R.id.chart);
////        Log.d("FRAGMENT", "FRAGMENT======================: " + graphActivityFragment);
//        fragmentManager.beginTransaction()
//                .add(R.id.overlay_fragment_container, new GraphActivity())
//                .commit();
    }

}
