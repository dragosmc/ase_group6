package com.six.group.gpsv4;

/**
 * Created by dragosmc on 11/11/2015.
 */
public class Property {

    public Double lat;
    public Double lng;
    public Double price;
    public Double weight;
    public String address;
    public String postcode;

    public Property(Double lat, Double lng, Double price, Double weight, String address, String postcode) {
        this.lat = lat;
        this.lng = lng;
        this.price = price;
        this.weight = weight;
        this.address = address;
        this.postcode = postcode;
    }
}
