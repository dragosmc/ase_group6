package com.six.group.gpsv4;

import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by mattpointon on 21/10/15.
 */
public class Position {


    private String deviceID;
    private String latitude;
    private String longitude;
    private HttpResponse response;

    public Position() {
        this.deviceID = "";
        this.longitude = "";
        this.latitude = "";

    }

    public Position(String androidKey, String latitude,String longtitude) {
        this.deviceID = androidKey;
        this.latitude = latitude;
        this.longitude = longtitude;

    }


    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }


    public String getLongtitude() {
        return longitude;
    }

    public void setLongtitude(String longtitude) {
        this.longitude = longtitude;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }


    @Override
    public String toString() {
        return "Position{" +
                "deviceID='" + deviceID + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }

    public HttpResponse sendPosition(Position position){
        int TIMEOUT_MILLISEC = 10000;  // = 10 seconds
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
        HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
        HttpClient client = new DefaultHttpClient(httpParams);
        Gson gson = new Gson();
        String jsonString = gson.toJson(position);
//        HttpPost post = new HttpPost("http://sussexgroup6.herokuapp.com/location");
        HttpGet post = new HttpGet("http://sussexgroup6.herokuapp.com/location");
        try {
            StringEntity se = new StringEntity(jsonString.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
//            post.setEntity(se);
            response = client.execute(post);
            return response;
        }
        catch(Exception e){
            Log.d("Exception", e.toString());
            return null;
        }
    }

    public int readResponse(HttpResponse response){
        try {
            InputStream reply = response.getEntity().getContent();
            Log.d("Response Position - ", response.getStatusLine().getStatusCode() + " " + responseToString(reply));
            return response.getStatusLine().getStatusCode();
        }
        catch(Exception e){
            Log.d("Exception", e.toString());
            return 0;
        }

    }

    public String responseToString(InputStream input){

        String line="";
        String data="";
        try{
            BufferedReader br=new BufferedReader(new InputStreamReader(input));
            while((line=br.readLine())!=null){
                data = data + line;
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return  data;
    }


}
