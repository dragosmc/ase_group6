package com.six.group.gpsv4;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.widget.Button;
import android.widget.TextView;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowIntent;
import org.robolectric.shadows.ShadowLocationManager;

import static android.location.LocationManager.GPS_PROVIDER;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, shadows = MainActivity.class)

public class MainActivityTest {
    private MainActivity mainActivity;
    private Button button;
    public LocationManager locationManager;
    private ShadowLocationManager shadowLocationManager;
    @Before
    public void setUp() {

        mainActivity = Robolectric.buildActivity(MainActivity.class).create().get();
        button = (Button) mainActivity.findViewById(R.id.showOnMapsBtn);
        locationManager = (LocationManager) RuntimeEnvironment.application.getSystemService(Context.LOCATION_SERVICE);
        shadowLocationManager = Shadows.shadowOf(locationManager);
        //mapsActivity = Robolectric.buildActivity(MapsActivity.class).create().get();
        // position = Robolectric.buildActivity(Position.class).create().get();


    }

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * -- TEST 2: SWITCH ACTIVITY --
     * Testing the onClick Event and whether a button click redirects to the maps activity
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */

    @Test
    public void testSecondActivityRedirection() throws Exception {

        button.performClick();
    }


    private void assertActivityStarted(Class<? extends Activity> MapsActivity) {
        ShadowActivity shadowActivity = Shadows.shadowOf(mainActivity);
        Intent startedIntent = shadowActivity.getNextStartedActivity();
        ShadowIntent shadowIntent = Shadows.shadowOf(startedIntent);
        Assert.assertEquals(MapsActivity.getName(), shadowIntent.getComponent().getClassName());
    }

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * -- TEST 2: VALUE EQUALITY TEST --
     * Testing of if a String is correct. Example includes: TextView String
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    @Test
    public void testTextView() {
        Activity activity = Robolectric.setupActivity(MainActivity.class);

        TextView results = (TextView) activity.findViewById(R.id.textView);
        String resultsText = results.getText().toString();

        // Examines if the text of the textView equals to the defined String
        Assert.assertEquals("Waiting for location...", resultsText);
    }

    @Test // Testing onDestroy method
    public void onDestroyTest() {
        mainActivity.finish();
    }

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * -- TEST 3: GPS/GPS Provider and LOCATION TESTS --
     * Testing GPS using a Mock location
     * [[ mapsActivity is also introduced as part of this test ]]
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */


    // Testing the GPS provider

    @Test
    public void shouldDisableProvider() {
        shadowLocationManager.setProviderEnabled(GPS_PROVIDER, false);

        Boolean enabled = locationManager.isProviderEnabled(GPS_PROVIDER);
        Assert.assertFalse(enabled);
    }

    @Test
    public void shouldReturnNullWhenBestProviderIsNotSet() throws Exception {
        Assert.assertNull(locationManager.getBestProvider(new Criteria(), true));
    }

    private GpsStatus.Listener addGpsListenerToLocationManager() {
        GpsStatus.Listener listener = new TestGpsListener();
        locationManager.addGpsStatusListener(listener);
        return listener;
    }

    private class TestGpsListener implements GpsStatus.Listener {

        @Override
        public void onGpsStatusChanged(int event) {

        }

        //Testing Mock Location
        @Test
        public void testMockLocation() {
            locationManager = (LocationManager) mainActivity.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        /* Adding some test criteria */
            locationManager.addTestProvider("Mock", false, false, false, false, false, false, false, Criteria.POWER_LOW, Criteria.ACCURACY_FINE);
            locationManager.setTestProviderEnabled("Mock", true);

            // Setting up the test
            Location location = new Location("Mock");
            location.setLatitude(10.0);
            location.setLongitude(20.0);
            locationManager.setTestProviderLocation("Mock", location);

            // Check if listener reacted the right way
            locationManager.removeTestProvider("Mock");
        }
    }
    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * -- Throwing exceptions for all methods/constructors of the MainActivity --
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */

        public void testOnCreate() throws Exception {

        }

        public void testOnDestroy() throws Exception {

        }

        public void testOpenInMaps() throws Exception {

        }

        public void testOnLocationChanged() throws Exception {

        }

        public void testOnStatusChanged() throws Exception {

        }

        public void testOnProviderEnabled() throws Exception {

        }

        public void testOnProviderDisabled() throws Exception {

        }

        public void testOnCreate1() throws Exception {

        }

        public void testOnPostCreate() throws Exception {

        }

        public void testGetSupportActionBar() throws Exception {

        }

        public void testSetSupportActionBar() throws Exception {

        }

        public void testGetMenuInflater() throws Exception {

        }

        public void testSetContentView() throws Exception {

        }

        public void testSetContentView1() throws Exception {

        }

        public void testSetContentView2() throws Exception {

        }

        public void testAddContentView() throws Exception {

        }

        public void testOnConfigurationChanged() throws Exception {

        }

        public void testOnStop() throws Exception {

        }

        public void testOnPostResume() throws Exception {

        }

        public void testOnMenuItemSelected() throws Exception {

        }

        public void testOnDestroy1() throws Exception {

        }

        public void testOnTitleChanged() throws Exception {

        }

        public void testSupportRequestWindowFeature() throws Exception {

        }

        public void testSupportInvalidateOptionsMenu() throws Exception {

        }

        public void testInvalidateOptionsMenu() throws Exception {

        }

        public void testOnSupportActionModeStarted() throws Exception {

        }

        public void testOnSupportActionModeFinished() throws Exception {

        }

        public void testOnWindowStartingSupportActionMode() throws Exception {

        }

        public void testStartSupportActionMode() throws Exception {

        }

        public void testSetSupportProgressBarVisibility() throws Exception {

        }

        public void testSetSupportProgressBarIndeterminateVisibility() throws Exception {

        }

        public void testSetSupportProgressBarIndeterminate() throws Exception {

        }

        public void testSetSupportProgress() throws Exception {

        }

        public void testOnCreateSupportNavigateUpTaskStack() throws Exception {

        }

        public void testOnPrepareSupportNavigateUpTaskStack() throws Exception {

        }

        public void testOnSupportNavigateUp() throws Exception {

        }

        public void testGetSupportParentActivityIntent() throws Exception {

        }

        public void testSupportShouldUpRecreateTask() throws Exception {

        }

        public void testSupportNavigateUpTo() throws Exception {

        }

        public void testOnContentChanged() throws Exception {

        }

        public void testOnSupportContentChanged() throws Exception {

        }

        public void testGetDrawerToggleDelegate() throws Exception {

        }

        public void testGetDelegate() throws Exception {

        }

        public void testOnActivityResult() throws Exception {

        }

        public void testOnBackPressed() throws Exception {

        }

        public void testSupportFinishAfterTransition() throws Exception {

        }

        public void testSetEnterSharedElementCallback() throws Exception {

        }

        public void testSetExitSharedElementCallback() throws Exception {

        }

        public void testSupportPostponeEnterTransition() throws Exception {

        }

        public void testSupportStartPostponedEnterTransition() throws Exception {

        }

        public void testOnConfigurationChanged1() throws Exception {

        }

        public void testOnCreate2() throws Exception {

        }

        public void testOnCreatePanelMenu() throws Exception {

        }

        public void testOnCreateView() throws Exception {

        }

        public void testOnDestroy2() throws Exception {

        }

        public void testOnKeyDown() throws Exception {

        }

        public void testOnLowMemory() throws Exception {

        }

        public void testOnMenuItemSelected1() throws Exception {

        }

        public void testOnPanelClosed() throws Exception {

        }

        public void testOnPause() throws Exception {

        }

        public void testOnNewIntent() throws Exception {

        }

        public void testOnResume() throws Exception {

        }

        public void testOnPostResume1() throws Exception {

        }

        public void testOnResumeFragments() throws Exception {

        }

        public void testOnPreparePanel() throws Exception {

        }

        public void testOnPrepareOptionsPanel() throws Exception {

        }

        public void testOnRetainNonConfigurationInstance() throws Exception {

        }
    }