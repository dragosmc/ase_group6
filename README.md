## Advanced Software Engineering  - Group 6 ##

https://github.com/PhilJay/MPAndroidChart
Used for the property graph.

### Task 3 deliverables ###

The code for this task can be found in the __master__ branch.

Heroku has been used for the cloud server. The location is sent in a JSON format to http://sussexgroup6.herokuapp.com/location as a POST HTTP request.

The code for the server is in the [ase_group6_server](https://bitbucket.org/SimonBidwell/ase_group6_server) repository.

### Task 2 deliverables ###

The code for this task can be found in the __rc1__ branch. 

The project plan can be found in the **/pdf/** directory in the same branch.

### Last update ###
22/10/2015